package com.example.game.dtos.response;

import lombok.Data;

@Data
public class StockResponse {

	private Long cantidad;
	
	public StockResponse() {}
}
