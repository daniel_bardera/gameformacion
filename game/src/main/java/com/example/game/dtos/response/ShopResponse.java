package com.example.game.dtos.response;

import lombok.Data;

@Data
public class ShopResponse {

	private String name;
	
	public ShopResponse(){}
}
