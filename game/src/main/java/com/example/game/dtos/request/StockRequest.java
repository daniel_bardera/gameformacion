package com.example.game.dtos.request;

import javax.validation.constraints.NotBlank;

import com.example.game.entities.Game;
import com.example.game.entities.Shop;

import lombok.Data;

@Data
public class StockRequest {

	@NotBlank(message = "Title cant be null or empty")
	private Game game;
	
	@NotBlank(message = "Title cant be null or empty")
	private Shop shop;
	
	@NotBlank(message = "Title cant be null or empty")
	private Long cantidad;
}
