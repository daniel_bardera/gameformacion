package com.example.game.convert;

import org.springframework.core.convert.converter.Converter;

import com.example.game.dtos.request.ShopRequest;
import com.example.game.entities.Shop;

public class ShopRequestToGameConverter implements Converter<ShopRequest, Shop>{

	public Shop convert(ShopRequest shopRequest) {
		Shop shop = new Shop();
		shop.setName(shopRequest.getName());
		
		return shop;
	}

}
