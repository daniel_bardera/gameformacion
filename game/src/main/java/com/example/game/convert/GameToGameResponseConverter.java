package com.example.game.convert;

import org.springframework.core.convert.converter.Converter;

import com.example.game.dtos.response.GameResponse;
import com.example.game.entities.Game;

public class GameToGameResponseConverter implements Converter<GameResponse, Game>{

	public Game convert(GameResponse gameResponse) {
		Game game = new Game();
		game.setTitle(gameResponse.getTitle());
		game.setRelease(gameResponse.getDate());
		return game;
	}
}
