package com.example.game.convert;

import org.springframework.core.convert.converter.Converter;

import com.example.game.entities.Genre;
import com.example.game.enums.GenderEnum;

public class GenreRequestToGenreConverter implements Converter<GenderEnum, Genre>{

	@Override
	public Genre convert(GenderEnum genderEnum) {
		Genre genre = new Genre();
		
		genre.setGenreName(genderEnum);	// Le decimos que nos traiga los generos
		return genre;
	}

	

	
}
