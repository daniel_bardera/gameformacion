package com.example.game.convert;


import org.springframework.core.convert.converter.Converter;

import com.example.game.dtos.request.StockRequest;
import com.example.game.entities.Stock;



public class StockRequestToStockConverter implements Converter<StockRequest, Stock>{

	
	
	@Override
	public Stock convert(StockRequest stockRequest) {
		Stock stock = new Stock();		
		
		stock.setGame(stockRequest.getGame());
		stock.setShop(stockRequest.getShop());
		stock.setCantidad(stockRequest.getCantidad());
		
		return stock;
	}

}
