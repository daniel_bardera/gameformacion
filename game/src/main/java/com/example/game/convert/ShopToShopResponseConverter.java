package com.example.game.convert;

import org.springframework.core.convert.converter.Converter;

import com.example.game.dtos.response.ShopResponse;
import com.example.game.entities.Shop;

public class ShopToShopResponseConverter implements Converter<ShopResponse, Shop>{

	public Shop convert(ShopResponse shopResponse) {
		Shop shop = new Shop();
		shop.setName(shopResponse.getName());
		
		return shop;
	}

}
