package com.example.game.convert;

import org.springframework.core.convert.converter.Converter;

import com.example.game.dtos.response.StockResponse;
import com.example.game.entities.Stock;

public class StockToStockResponseConverter implements Converter<StockResponse, Stock>{

	@Override
	public Stock convert(StockResponse stockResponse) {
		Stock stock = new Stock();
		stock.setCantidad(stockResponse.getCantidad());
				
		return stock;
	}

}
