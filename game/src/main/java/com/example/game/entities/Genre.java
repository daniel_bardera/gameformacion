package com.example.game.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.example.game.enums.GenderEnum;

import lombok.Data;


@Entity
@Table(name = "GENRES")
@Data
public class Genre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "GENRE")
	private GenderEnum genreName;
	
	@ManyToMany(mappedBy= "genres")	// Ponemos el mapped a la tabla que no vamos a consultar
	private List<Game> games;
	

}
