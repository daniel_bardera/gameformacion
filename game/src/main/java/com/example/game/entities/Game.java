package com.example.game.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "GAMES")
@Data
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@ManyToMany(cascade= {CascadeType.PERSIST, CascadeType.MERGE})	// Creamos una relacion con la entidad Genre
	private List<Genre> genres = new ArrayList<>();
	
	@OneToMany(mappedBy= "game")
	private List<Stock> stocks;
	
	@Column(name = "RELEASE")
	private Date release;
}
