package com.example.game.services;

import com.example.game.entities.Genre;
import com.example.game.enums.GenderEnum;

public interface GenreService {

	public Genre findGenreBy(GenderEnum genre);
}
