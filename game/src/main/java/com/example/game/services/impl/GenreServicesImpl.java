package com.example.game.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.game.convert.GenreRequestToGenreConverter;
import com.example.game.entities.Genre;
import com.example.game.enums.GenderEnum;
import com.example.game.exceptions.GenreNotFoundException;
import com.example.game.repository.GenreRepository;
import com.example.game.services.GenreService;

@Service
public class GenreServicesImpl implements GenreService{

	@Autowired
	private GenreRepository genreRepo;
	
	@Override
	public Genre findGenreBy(GenderEnum genreEnum) {
		Optional<Genre> genre = genreRepo.findByGenreName(genreEnum);

		if (genre.isPresent()) {
			return genre.get();
		}else
			throw new GenreNotFoundException();
	}
	
	//Pre-cargamos los géneros en la BBDD
	@PostConstruct	
	public void loadGenres() {
		
		// Convertimos los géneros
		GenreRequestToGenreConverter genreEnumToGenre = new GenreRequestToGenreConverter();
		
		List<Genre> genres = new ArrayList<>();
		
		for(int i=0; i< GenderEnum.values().length; i++) {
			if(!genreRepo.findByGenreName(GenderEnum.values()[i]).isPresent()) {
				genres.add(genreEnumToGenre.convert(GenderEnum.values()[i]));
			}
		}
		genreRepo.saveAll(genres);
	}

}
