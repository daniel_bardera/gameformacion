package com.example.game.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.example.game.dtos.request.ShopRequest;
import com.example.game.dtos.response.ShopResponse;
import com.example.game.entities.Shop;
import com.example.game.exceptions.ShopNotFoundException;
import com.example.game.repository.ShopRepository;
import com.example.game.services.ShopService;

@Service
public class ShopServicesImpl implements ShopService {

	@Autowired
	private ShopRepository shopRepo;
	
	@Autowired
	private ConversionService converter;
	
	@Override
	public ShopResponse addShop(ShopRequest shopRequest) {
		Shop shop = converter.convert(shopRequest, Shop.class);
		shopRepo.save(shop);
		return new ShopResponse();
	}

	@Override
	public ShopResponse getShop(String name) {
		Optional<Shop> shop = shopRepo.findByName(name);
		if(shop.isPresent()) {
			return converter.convert(shop.get(), ShopResponse.class);
		}
		else
			throw new ShopNotFoundException();
	}
}


