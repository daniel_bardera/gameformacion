package com.example.game.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.example.game.dtos.request.StockRequest;
import com.example.game.dtos.response.StockResponse;
import com.example.game.entities.Stock;
import com.example.game.helper.StockHelper;
import com.example.game.repository.StockRepository;
import com.example.game.services.StockService;

@Service
public class StockServiceImpl implements StockService{

	@Autowired
	private StockRepository stockRepo;
	
	@Autowired
	private StockHelper stockHelper;
	
	@Autowired
	private ConversionService converter;
	
	@Override
	public StockResponse addStock(StockRequest stockRequest) {
		
		Optional<Stock> game = stockRepo.findByGameTitle(stockRequest.getGame().getTitle());
		
		if(!game.isPresent()) {
			Stock stock = stockHelper.convertStockRequestToStock(stockRequest);
			stockRepo.save(stock);
			return new StockResponse();
		}
		game.get().setCantidad(stockRequest.getCantidad());
		
		stockRepo.saveAndFlush(game.get());
		
		return new StockResponse();
	}

	
}
