package com.example.game.services.impl;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.example.game.dtos.request.GameRequest;
import com.example.game.dtos.response.GameResponse;
import com.example.game.entities.Game;
import com.example.game.exceptions.GameKONotFoundException;
import com.example.game.exceptions.GameNotFoundException;
import com.example.game.helper.GameHelper;
import com.example.game.repository.GameRepository;
import com.example.game.services.GameService;


@Service
public class GameServiceImpl implements GameService{
	
	@Autowired
	private GameHelper gameHelper;
	
	@Autowired
	private GameRepository gameRepo;
		
	@Autowired
	private ConversionService converter;

	// Añade el juego
	@Override
	public GameResponse addGame(GameRequest gameRequest) {
		Game game = gameHelper.convertGameRequestToGame(gameRequest);
		gameRepo.save(game);	// Guardamos en la BBDD
		return new GameResponse();
	}
	
	// Busca el juego por el título	
	@Override
	public GameResponse getGame(String title) {
		Optional<Game> game = gameRepo.findByTitle(title);
		if(game.isPresent()) {
			return converter.convert(game.get(), GameResponse.class);
		}
		else
			throw new GameKONotFoundException();
	}

	@Override
	public GameResponse delete(String title) {
		Optional<Game> game = gameRepo.findByTitle(title);
		if(game.isPresent()) {
			gameRepo.deleteByTitle(title);
			return converter.convert(game.get(), GameResponse.class);
		}
		else
			throw new GameNotFoundException();
		
	}

	@Override
	public GameResponse updateGame(GameRequest gameRequest) {
		Optional<Game> gameSearch = gameRepo.findByTitle(gameRequest.getTitle());
		
		gameSearch.get().setTitle(gameRequest.getTitle());
		gameSearch.get().setDescription(gameRequest.getDescription());
		gameSearch.get().setRelease(gameRequest.getRelease());
		
		Game game = gameRepo.saveAndFlush(gameSearch.get());
	
		return new GameResponse();
	}
}


