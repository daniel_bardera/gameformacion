package com.example.game.exceptions;

import com.example.game.exceptions.generic.GameKOException;

public class GenreNotFoundException extends GameKOException{

private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "El genero no se encuentra en la BBDD";
	
	public GenreNotFoundException(String detalle) {		
		super(detalle);
	}

	public GenreNotFoundException() {
		super(DETAIL);
	}
}