package com.example.game.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.game.entities.Game;
import com.example.game.entities.Shop;
import com.example.game.entities.Stock;

public interface StockRepository extends JpaRepository<Stock, Long>{

	//public Optional<Stock> findByShopAndGame(Shop shop, Game game);
	
	public Optional<Stock> findById(Long id);
	
	public Optional<Stock> findByGameTitle(String title);
}
