package com.example.game.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.game.entities.Genre;
import com.example.game.enums.GenderEnum;

public interface GenreRepository extends JpaRepository<Genre, Long>{

	public Optional <Genre> findByGenreName(GenderEnum genderEnum);
}
