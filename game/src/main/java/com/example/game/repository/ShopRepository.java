package com.example.game.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.game.entities.Shop;

@Repository
public interface ShopRepository extends JpaRepository<Shop, Long>{

	public Optional<Shop> findByName(String name);
}
