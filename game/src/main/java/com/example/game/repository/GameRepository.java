package com.example.game.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.game.entities.Game;

@Repository
public interface GameRepository extends JpaRepository<Game, Long>{
	
	// Aquí vamos a realizar nuestras "consultas"
	
	// ORM: pasamos un objeto como si fuera una tabla
	// Elaboramos un metodo find para buscar por titulo
	public Optional<Game> findByTitle(String title);
	
	public Optional<Game> deleteByTitle(String title);
}
