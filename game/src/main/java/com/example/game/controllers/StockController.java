package com.example.game.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.game.dtos.request.StockRequest;
import com.example.game.dtos.response.StockResponse;
import com.example.game.services.StockService;

@RestController
public class StockController {

	@Autowired
	private StockService stockService;
	
	@PostMapping("/stock")
	public ResponseEntity<Object> addStock(@RequestBody @Valid StockRequest stockRequest, HttpServletRequest request){
		StockResponse stockResponse = stockService.addStock(stockRequest);
		return ResponseEntity.status(HttpStatus.OK).body(stockResponse);
	}
}
