package com.example.game.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.example.game.convert.GameRequestToGameConverter;
import com.example.game.convert.GameToGameResponseConverter;
import com.example.game.convert.GenreRequestToGenreConverter;
import com.example.game.convert.ShopRequestToGameConverter;
import com.example.game.convert.ShopToShopResponseConverter;
import com.example.game.convert.StockRequestToStockConverter;
import com.example.game.convert.StockToStockResponseConverter;


@Configuration
public class ConverterConfig implements WebMvcConfigurer{

		public void addFormatters(FormatterRegistry registry) {
			registry.addConverter(new GameRequestToGameConverter());
			registry.addConverter(new GameToGameResponseConverter());
			registry.addConverter(new GenreRequestToGenreConverter());
			registry.addConverter(new ShopRequestToGameConverter());
			registry.addConverter(new ShopToShopResponseConverter());
			registry.addConverter(new StockRequestToStockConverter());
			registry.addConverter(new StockToStockResponseConverter());
		}
}
