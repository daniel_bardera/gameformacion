package com.example.game.helper;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import com.example.game.dtos.request.GameRequest;
import com.example.game.entities.Game;
import com.example.game.entities.Genre;
import com.example.game.enums.GenderEnum;
import com.example.game.services.GenreService;


@Service
public class GameHelper {

	@Autowired
	private ConversionService converter;
		
	@Autowired
	private GenreService genreService;
	
	
	public Game convertGameRequestToGame(GameRequest gameRequest) {
		Game game = converter.convert(gameRequest, Game.class);
		// El stream unido al map nos indica que a cada genero lo convertimos y guardamos en una coleccion
		//List<Genre> genres = gameRequest.getGenre().stream().map(gr -> converter.convert(gr, Genre.class)).collect(Collectors.toList());
		//game.getGenres().addAll(genres);
				
		for(GenderEnum genreRequest : gameRequest.getGenre()) {
			Genre genre = genreService.findGenreBy(genreRequest);
			game.getGenres().add(genre);
		}
		
		return game;
	}
	
}
