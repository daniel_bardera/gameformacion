package com.example.game.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.example.game.dtos.request.StockRequest;
import com.example.game.entities.Stock;

@Service
public class StockHelper {

	@Autowired
	private ConversionService converter;

	public Stock convertStockRequestToStock(StockRequest stockRequest) {
		Stock stock = converter.convert(stockRequest, Stock.class);
		return stock;
			
	}
}
